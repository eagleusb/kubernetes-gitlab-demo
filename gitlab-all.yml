apiVersion: v1
kind: Namespace
metadata:
  name: kube-lego
---
apiVersion: v1
metadata:
  name: kube-lego
  namespace: kube-lego
data:
  # modify this to specify your address
  lego.email: "kamil@gitlab.com"
  # configure letencrypt's production api
  lego.url: "https://acme-v01.api.letsencrypt.org/directory"
kind: ConfigMap
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: kube-lego
  namespace: kube-lego
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: kube-lego
    spec:
      containers:
      - name: kube-lego
        image: jetstack/kube-lego:0.1.3
        imagePullPolicy: Always
        ports:
        - containerPort: 8080
        env:
        - name: LEGO_EMAIL
          valueFrom:
            configMapKeyRef:
              name: kube-lego
              key: lego.email
        - name: LEGO_URL
          valueFrom:
            configMapKeyRef:
              name: kube-lego
              key: lego.url
        - name: LEGO_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: LEGO_POD_IP
          valueFrom:
            fieldRef:
              fieldPath: status.podIP
        readinessProbe:
          httpGet:
            path: /healthz
            port: 8080
          initialDelaySeconds: 5
          timeoutSeconds: 1
---
apiVersion: v1
kind: Namespace
metadata:
  name: nginx-ingress
---
apiVersion: v1
data:
  proxy-connect-timeout: "15"
  proxy-read-timeout: "600"
  proxy-send-imeout: "600"
  hsts-include-subdomains: "false"
  body-size: "512m"
  server-name-hash-bucket-size: "256"
kind: ConfigMap
metadata:
  namespace: nginx-ingress
  name: nginx
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: default-http-backend
  namespace: nginx-ingress
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: default-http-backend
    spec:
      containers:
      - name: default-http-backend
        # Any image is permissable as long as:
        # 1. It serves a 404 page at /
        # 2. It serves 200 on a /healthz endpoint
        image: gcr.io/google_containers/defaultbackend:1.0
        livenessProbe:
          httpGet:
            path: /healthz
            port: 8080
            scheme: HTTP
          initialDelaySeconds: 30
          timeoutSeconds: 5
        ports:
        - containerPort: 8080
        resources:
          limits:
            cpu: 10m
            memory: 20Mi
          requests:
            cpu: 10m
            memory: 20Mi
---
apiVersion: v1
kind: Service
metadata:
  name: default-http-backend
  namespace: nginx-ingress
spec:
  ports:
  - port: 80
    targetPort: 8080
    protocol: TCP
  selector:
    app: default-http-backend
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: nginx
  namespace: nginx-ingress
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - image: gcr.io/google_containers/nginx-ingress-controller:0.8.3
        name: nginx
        imagePullPolicy: Always
        env:
          - name: POD_NAME
            valueFrom:
              fieldRef:
                fieldPath: metadata.name
          - name: POD_NAMESPACE
            valueFrom:
              fieldRef:
                fieldPath: metadata.namespace
        livenessProbe:
          httpGet:
            path: /healthz
            port: 10254
            scheme: HTTP
          initialDelaySeconds: 30
          timeoutSeconds: 5
        ports:
        - containerPort: 80
        - containerPort: 443
        args:
        - /nginx-ingress-controller
        - --default-backend-service=nginx-ingress/default-http-backend
        - --nginx-configmap=nginx-ingress/nginx
---
apiVersion: v1
kind: Service
metadata:
  name: nginx
  namespace: nginx-ingress
spec:
  type: LoadBalancer
  loadBalancerIP: 104.155.5.78
  ports:
  - port: 80
    name: http
  - port: 443
    name: https
  selector:
    app: nginx
---
kind: Namespace
apiVersion: v1
metadata:
  name: gitlab
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: gitlab-config
  namespace: gitlab
data:
  external_scheme: https
  external_hostname: gitlab.tanuki.ayucloud.ovh
  registry_external_scheme: https
  registry_external_hostname: registry.tanuki.ayucloud.ovh
  mattermost_external_scheme: https
  mattermost_external_hostname: mattermost.tanuki.ayucloud.ovh
  mattermost_app_uid: aadas
  postgres_user: gitlab
  postgres_db: gitlab_production
---
apiVersion: v1
kind: Secret
metadata:
  name: gitlab-secrets
  namespace: gitlab
data:
  postgres_password: NDl1ZjNtenMxcWR6NXZnbw==
  initial_shared_runners_registration_token: NDl1ZjNtenMxcWR6NXZnbw==
  mattermost_app_secret: NDl1ZjNtenMxcWR6NXZnbw==
---
kind: StorageClass
apiVersion: storage.k8s.io/v1beta1
metadata:
  name: fast
  namespace: gitlab
  annotations:
    storageclass.beta.kubernetes.io/is-default-class: "true"
  labels:
    kubernetes.io/cluster-service: "true"
provisioner: kubernetes.io/gce-pd
parameters:
  type: pd-ssd
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: gitlab-redis
  namespace: gitlab
spec:
  replicas: 1
  template:
    metadata:
      labels:
        name: gitlab-redis
    spec:
      containers:
      - name: redis
        image: redis:3.2.4
        imagePullPolicy: IfNotPresent
        ports:
        - name: redis
          containerPort: 6379
        volumeMounts:
        - mountPath: /var/lib/redis
          name: data
        livenessProbe:
          exec:
            command:
            - redis-cli
            - ping
          initialDelaySeconds: 30
          timeoutSeconds: 5
        readinessProbe:
          exec:
            command:
            - redis-cli
            - ping
          initialDelaySeconds: 5
          timeoutSeconds: 1
      volumes:
      - name: data
        persistentVolumeClaim:
          claimName: gitlab-redis-storage
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: gitlab-redis-storage
  namespace: gitlab
  annotations:
    volume.beta.kubernetes.io/storage-class: fast
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 5Gi
---
apiVersion: v1
kind: Service
metadata:
  name: gitlab-redis
  namespace: gitlab
  labels:
    name: gitlab-redis
spec:
  selector:
    name: gitlab-redis
  ports:
    - name: redis
      port: 6379
      targetPort: redis
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: gitlab-postgresql-initdb
  namespace: gitlab
data:
  01_create_mattermost_production.sql: |
    CREATE DATABASE mattermost_production WITH OWNER gitlab;
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: gitlab-postgresql
  namespace: gitlab
spec:
  replicas: 1
  template:
    metadata:
      labels:
        name: gitlab-postgresql
    spec:
      containers:
      - name: postgresql
        image: postgres:9.5.3
        imagePullPolicy: IfNotPresent
        env:
        - name: POSTGRES_USER
          valueFrom:
            configMapKeyRef:
              name: gitlab-config
              key: postgres_user
        - name: POSTGRES_PASSWORD
          valueFrom:
            secretKeyRef:
              name: gitlab-secrets
              key: postgres_password
        - name: POSTGRES_DB
          valueFrom:
            configMapKeyRef:
              name: gitlab-config
              key: postgres_db
        - name: DB_EXTENSION
          value: pg_trgm
        ports:
        - name: postgres
          containerPort: 5432
        volumeMounts:
        - mountPath: /var/lib/postgresql
          name: data
        - mountPath: /docker-entrypoint-initdb.d
          name: initdb
          readOnly: true
        livenessProbe:
          exec:
            command:
            - pg_isready
            - -h
            - localhost
            - -U
            - postgres
          initialDelaySeconds: 30
          timeoutSeconds: 5
        readinessProbe:
          exec:
            command:
            - pg_isready
            - -h
            - localhost
            - -U
            - postgres
          initialDelaySeconds: 5
          timeoutSeconds: 1
      volumes:
      - name: data
        persistentVolumeClaim:
          claimName: gitlab-postgresql-storage
      - name: initdb
        configMap:
          name: gitlab-postgresql-initdb
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: gitlab-postgresql-storage
  namespace: gitlab
  annotations:
    volume.beta.kubernetes.io/storage-class: fast
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 30Gi
---
apiVersion: v1
kind: Service
metadata:
  name: gitlab-postgresql
  namespace: gitlab
  labels:
    name: gitlab-postgresql
spec:
  ports:
    - name: postgres
      port: 5432
      targetPort: postgres
  selector:
    name: gitlab-postgresql
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: gitlab-config-storage
  namespace: gitlab
  annotations:
    volume.beta.kubernetes.io/storage-class: fast
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: gitlab
  namespace: gitlab
spec:
  replicas: 1
  template:
    metadata:
      labels:
        name: gitlab
        app: gitlab
    spec:
      containers:
      - name: gitlab
        image: gitlab/gitlab-ce:8.15.4-ce.1
        imagePullPolicy: IfNotPresent
        command: ["/bin/bash", "-c",
          "patch -p1 -d /opt/gitlab/embedded/service/gitlab-rails < /patches/add-kubernetes-deploy.patch && patch -p1 -d /opt/gitlab/embedded/service/gitlab-rails < /patches/fix-git-hooks.patch && sed -i \"s/environment ({'GITLAB_ROOT_PASSWORD' => initial_root_password }) if initial_root_password/environment ({'GITLAB_ROOT_PASSWORD' => initial_root_password, 'GITLAB_SHARED_RUNNERS_REGISTRATION_TOKEN' => node['gitlab']['gitlab-rails']['initial_shared_runners_registration_token'] })/g\" /opt/gitlab/embedded/cookbooks/gitlab/recipes/database_migrations.rb && exec /assets/wrapper"]
        env:
        - name: GITLAB_EXTERNAL_SCHEME
          valueFrom:
            configMapKeyRef:
              name: gitlab-config
              key: external_scheme
        - name: GITLAB_EXTERNAL_HOSTNAME
          valueFrom:
            configMapKeyRef:
              name: gitlab-config
              key: external_hostname
        - name: GITLAB_REGISTRY_EXTERNAL_SCHEME
          valueFrom:
            configMapKeyRef:
              name: gitlab-config
              key: registry_external_scheme
        - name: GITLAB_REGISTRY_EXTERNAL_HOSTNAME
          valueFrom:
            configMapKeyRef:
              name: gitlab-config
              key: registry_external_hostname
        - name: GITLAB_MATTERMOST_EXTERNAL_SCHEME
          valueFrom:
            configMapKeyRef:
              name: gitlab-config
              key: mattermost_external_scheme
        - name: GITLAB_MATTERMOST_EXTERNAL_HOSTNAME
          valueFrom:
            configMapKeyRef:
              name: gitlab-config
              key: mattermost_external_hostname
        - name: POSTGRES_USER
          valueFrom:
            configMapKeyRef:
              name: gitlab-config
              key: postgres_user
        - name: POSTGRES_PASSWORD
          valueFrom:
            secretKeyRef:
              name: gitlab-secrets
              key: postgres_password
        - name: POSTGRES_DB
          valueFrom:
            configMapKeyRef:
              name: gitlab-config
              key: postgres_db
        - name: GITLAB_INITIAL_SHARED_RUNNERS_REGISTRATION_TOKEN
          valueFrom:
            secretKeyRef:
              name: gitlab-secrets
              key: initial_shared_runners_registration_token
        - name: MATTERMOST_APP_UID
          valueFrom:
            configMapKeyRef:
              name: gitlab-config
              key: mattermost_app_uid
        - name: MATTERMOST_APP_SECRET
          valueFrom:
            secretKeyRef:
              name: gitlab-secrets
              key: mattermost_app_secret
        - name: GITLAB_OMNIBUS_CONFIG
          value: |
            external_url "#{ENV['GITLAB_EXTERNAL_SCHEME']}://#{ENV['GITLAB_EXTERNAL_HOSTNAME']}"
            registry_external_url "#{ENV['GITLAB_REGISTRY_EXTERNAL_SCHEME']}://#{ENV['GITLAB_REGISTRY_EXTERNAL_HOSTNAME']}"
            mattermost_external_url "#{ENV['GITLAB_MATTERMOST_EXTERNAL_SCHEME']}://#{ENV['GITLAB_MATTERMOST_EXTERNAL_HOSTNAME']}"

            gitlab_rails['initial_shared_runners_registration_token'] = ENV['GITLAB_INITIAL_SHARED_RUNNERS_REGISTRATION_TOKEN']

            nginx['enable'] = false
            registry_nginx['enable'] = false
            mattermost_nginx['enable'] = false

            gitlab_workhorse['listen_network'] = 'tcp'
            gitlab_workhorse['listen_addr'] = '0.0.0.0:8005'

            mattermost['service_address'] = '0.0.0.0'
            mattermost['service_port'] = '8065'

            registry['registry_http_addr'] = '0.0.0.0:8105'

            postgresql['enable'] = false
            gitlab_rails['db_host'] = 'gitlab-postgresql'
            gitlab_rails['db_password'] = ENV['POSTGRES_PASSWORD']
            gitlab_rails['db_username'] = ENV['POSTGRES_USER']
            gitlab_rails['db_database'] = ENV['POSTGRES_DB']

            redis['enable'] = false
            gitlab_rails['redis_host'] = 'gitlab-redis'

            mattermost['file_directory'] = '/gitlab-data/mattermost';
            mattermost['sql_driver_name'] = 'postgres';
            mattermost['sql_data_source'] = "user=#{ENV['POSTGRES_USER']} host=gitlab-postgresql port=5432 dbname=mattermost_production password=#{ENV['POSTGRES_PASSWORD']} sslmode=disable";
            mattermost['gitlab_enable'] = true;
            mattermost['gitlab_secret'] = ENV['MATTERMOST_APP_SECRET'];
            mattermost['gitlab_id'] = ENV['MATTERMOST_APP_UID'];
            mattermost['gitlab_scope'] = '';
            mattermost['gitlab_auth_endpoint'] = "#{ENV['GITLAB_EXTERNAL_SCHEME']}://#{ENV['GITLAB_EXTERNAL_HOSTNAME']}/oauth/authorize";
            mattermost['gitlab_token_endpoint'] = "#{ENV['GITLAB_EXTERNAL_SCHEME']}://#{ENV['GITLAB_EXTERNAL_HOSTNAME']}/oauth/token";
            mattermost['gitlab_user_api_endpoint'] = "#{ENV['GITLAB_EXTERNAL_SCHEME']}://#{ENV['GITLAB_EXTERNAL_HOSTNAME']}/api/v3/user"

            manage_accounts['enable'] = true
            manage_storage_directories['manage_etc'] = false

            gitlab_shell['auth_file'] = '/gitlab-data/authorized_keys'
            git_data_dir '/gitlab-data/git-data'
            gitlab_rails['shared_path'] = '/gitlab-data/shared'
            gitlab_rails['uploads_directory'] = '/gitlab-data/uploads'
            gitlab_ci['builds_directory'] = '/gitlab-data/builds'
            gitlab_rails['registry_path'] = '/gitlab-registry'
        - name: GITLAB_POST_RECONFIGURE_SCRIPT
          value: |
            /opt/gitlab/bin/gitlab-rails runner -e production 'Doorkeeper::Application.where(uid: ENV["MATTERMOST_APP_UID"], secret: ENV["MATTERMOST_APP_SECRET"], redirect_uri: "#{ENV["GITLAB_MATTERMOST_EXTERNAL_SCHEME"]}://#{ENV["GITLAB_MATTERMOST_EXTERNAL_HOSTNAME"]}/signup/gitlab/complete\r\n#{ENV["GITLAB_MATTERMOST_EXTERNAL_SCHEME"]}://#{ENV["GITLAB_MATTERMOST_EXTERNAL_HOSTNAME"]}/login/gitlab/complete", name: "GitLab Mattermost").first_or_create;'
        ports:
        - name: registry
          containerPort: 8105
        - name: mattermost
          containerPort: 8065
        - name: workhorse
          containerPort: 8005
        - name: ssh
          containerPort: 22
        volumeMounts:
        - name: config
          mountPath: /etc/gitlab
        - name: data
          mountPath: /gitlab-data
        - name: registry
          mountPath: /gitlab-registry
        - name: patches
          mountPath: /patches
          readOnly: true
        livenessProbe:
          httpGet:
            path: /help
            port: 8005
          initialDelaySeconds: 180
          timeoutSeconds: 15
        readinessProbe:
          httpGet:
            path: /help
            port: 8005
          initialDelaySeconds: 15
          timeoutSeconds: 1
      volumes:
      - name: data
        persistentVolumeClaim:
          claimName: gitlab-rails-storage
      - name: registry
        persistentVolumeClaim:
          claimName: gitlab-registry-storage
      - name: config
        persistentVolumeClaim:
          claimName: gitlab-config-storage
      - name: patches
        configMap:
          name: gitlab-patches
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: gitlab-patches
  namespace: gitlab
data:
  add-kubernetes-deploy.patch: |
    diff --git a/vendor/gitlab-ci-yml/autodeploy/Kubernetes.gitlab-ci.yml b/vendor/gitlab-ci-yml/autodeploy/Kubernetes.gitlab-ci.yml
    new file mode 100644
    index 0000000000..e384b585ae
    --- /dev/null
    +++ b/vendor/gitlab-ci-yml/autodeploy/Kubernetes.gitlab-ci.yml
    @@ -0,0 +1,74 @@
    +image: registry.gitlab.com/gitlab-examples/kubernetes-deploy
    +
    +variables:
    +  # Application deployment domain
    +  KUBE_DOMAIN: domain.example.com
    +
    +stages:
    +  - build
    +  - test
    +  - review
    +  - staging
    +  - production
    +
    +build:
    +  stage: build
    +  script:
    +    - command build
    +  only:
    +    - branches
    +
    +production:
    +  stage: production
    +  variables:
    +    CI_ENVIRONMENT_URL: http://production.$KUBE_DOMAIN
    +  script:
    +    - command deploy
    +  environment:
    +    name: production
    +    url: http://production.$KUBE_DOMAIN
    +  when: manual
    +  only:
    +    - master
    +
    +staging:
    +  stage: staging
    +  variables:
    +    CI_ENVIRONMENT_URL: http://staging.$KUBE_DOMAIN
    +  script:
    +    - command deploy
    +  environment:
    +    name: staging
    +    url: http://staging.$KUBE_DOMAIN
    +  only:
    +    - master
    +
    +review:
    +  stage: review
    +  variables:
    +    CI_ENVIRONMENT_URL: http://$CI_ENVIRONMENT_SLUG.$KUBE_DOMAIN
    +  script:
    +    - command deploy
    +  environment:
    +    name: review/$CI_BUILD_REF_NAME
    +    url: http://$CI_ENVIRONMENT_SLUG.$KUBE_DOMAIN
    +    on_stop: stop_review
    +  only:
    +    - branches
    +  except:
    +    - master
    +
    +stop_review:
    +  stage: review
    +  variables:
    +    GIT_STRATEGY: none
    +  script:
    +    - command destroy
    +  environment:
    +    name: review/$CI_BUILD_REF_NAME
    +    action: stop
    +  when: manual
    +  only:
    +    - branches
    +  except:
    +    - master
  fix-git-hooks.patch: |
    diff --git a/app/models/repository.rb b/app/models/repository.rb
    index 30be726243..0776c7ccc5 100644
    --- a/app/models/repository.rb
    +++ b/app/models/repository.rb
    @@ -160,14 +160,18 @@ class Repository
         tags.find { |tag| tag.name == name }
       end

    -  def add_branch(user, branch_name, target)
    +  def add_branch(user, branch_name, target, with_hooks: true)
         oldrev = Gitlab::Git::BLANK_SHA
         ref    = Gitlab::Git::BRANCH_REF_PREFIX + branch_name
         target = commit(target).try(:id)

         return false unless target

    -    GitHooksService.new.execute(user, path_to_repo, oldrev, target, ref) do
    +    if with_hooks
    +      GitHooksService.new.execute(user, path_to_repo, oldrev, target, ref) do
    +        update_ref!(ref, target, oldrev)
    +      end
    +    else
           update_ref!(ref, target, oldrev)
         end

    diff --git a/app/services/commits/change_service.rb b/app/services/commits/change_service.rb
    index 1c82599c57..2d4c9788d0 100644
    --- a/app/services/commits/change_service.rb
    +++ b/app/services/commits/change_service.rb
    @@ -55,7 +55,7 @@ module Commits
           return success if repository.find_branch(new_branch)

           result = CreateBranchService.new(@project, current_user)
    -                                  .execute(new_branch, @target_branch, source_project: @source_project)
    +                                  .execute(new_branch, @target_branch, source_project: @source_project, with_hooks: false)

           if result[:status] == :error
             raise ChangeError, "There was an error creating the source branch: #{result[:message]}"
    diff --git a/app/services/create_branch_service.rb b/app/services/create_branch_service.rb
    index 757fc35a78..a6a3461e17 100644
    --- a/app/services/create_branch_service.rb
    +++ b/app/services/create_branch_service.rb
    @@ -1,5 +1,5 @@
     class CreateBranchService < BaseService
    -  def execute(branch_name, ref, source_project: @project)
    +  def execute(branch_name, ref, source_project: @project, with_hooks: true)
         valid_branch = Gitlab::GitRefValidator.validate(branch_name)

         unless valid_branch
    @@ -26,7 +26,7 @@ class CreateBranchService < BaseService

                        repository.find_branch(branch_name)
                      else
    -                   repository.add_branch(current_user, branch_name, ref)
    +                   repository.add_branch(current_user, branch_name, ref, with_hooks: with_hooks)
                      end

         if new_branch
    diff --git a/app/services/files/base_service.rb b/app/services/files/base_service.rb
    index 9bd4bd464f..1802b932e0 100644
    --- a/app/services/files/base_service.rb
    +++ b/app/services/files/base_service.rb
    @@ -74,7 +74,7 @@ module Files
         end

         def create_target_branch
    -      result = CreateBranchService.new(project, current_user).execute(@target_branch, @source_branch, source_project: @source_project)
    +      result = CreateBranchService.new(project, current_user).execute(@target_branch, @source_branch, source_project: @source_project, with_hooks: false)

           unless result[:status] == :success
             raise_error("Something went wrong when we tried to create #{@target_branch} for you: #{result[:message]}")
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: gitlab-rails-storage
  namespace: gitlab
  annotations:
    volume.beta.kubernetes.io/storage-class: fast
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 30Gi
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: gitlab-registry-storage
  namespace: gitlab
  annotations:
    volume.beta.kubernetes.io/storage-class: fast
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 30Gi
---
apiVersion: v1
kind: Service
metadata:
  name: gitlab
  namespace: gitlab
  labels:
    name: gitlab
spec:
  selector:
    name: gitlab
  ports:
    - name: ssh
      port: 22
      targetPort: ssh
    - name: mattermost
      port: 8065
      targetPort: mattermost
    - name: registry
      port: 8105
      targetPort: registry
    - name: workhorse
      port: 8005
      targetPort: workhorse
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: gitlab-runner-scripts
  namespace: gitlab
data:
  entrypoint: |
    #!/bin/bash

    set -xe

    cp /scripts/config.toml /etc/gitlab-runner/

    # Register the runner
    /entrypoint register --non-interactive \
      --url http://gitlab.gitlab:8005/ci \
      --executor kubernetes \
      --kubernetes-privileged=true

    # Start the runner
    /entrypoint run --user=gitlab-runner \
      --working-directory=/home/gitlab-runner
  config.toml: |
    concurrent = 10
    check_interval = 1
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: gitlab-runner-docker
  namespace: gitlab
spec:
  replicas: 1
  template:
    metadata:
      labels:
        name: docker-runner
        app: gitlab-runner
    spec:
      containers:
      - name: gitlab-runner-docker
        image: gitlab/gitlab-runner:alpine-v1.9.2
        command: ["/bin/bash", "/scripts/entrypoint"]
        imagePullPolicy: IfNotPresent
        env:
        - name: REGISTRATION_TOKEN
          valueFrom:
            secretKeyRef:
              name: gitlab-secrets
              key: initial_shared_runners_registration_token
        resources:
          limits:
            memory: 500Mi
            cpu: 600m
          requests:
            memory: 500Mi
            cpu: 600m
        volumeMounts:
        - name: scripts
          mountPath: /scripts
        - name: var-run-docker-sock
          mountPath: /var/run/docker.sock
      volumes:
      - name: var-run-docker-sock
        hostPath:
          path: /var/run/docker.sock
      - name: scripts
        configMap:
          name: gitlab-runner-scripts
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: gitlab
  namespace: gitlab
  labels:
    name: gitlab
  annotations:
    kubernetes.io/tls-acme: "true"
    kubernetes.io/ingress.class: "nginx"
spec:
  tls:
  - hosts:
    - gitlab.tanuki.ayucloud.ovh
    - registry.tanuki.ayucloud.ovh
    - mattermost.tanuki.ayucloud.ovh
    secretName: gitlab-tls
  rules:
  - host: gitlab.tanuki.ayucloud.ovh
    http:
      paths:
      - path: /
        backend:
          serviceName: gitlab
          servicePort: 8005
  - host: registry.tanuki.ayucloud.ovh
    http:
      paths:
      - path: /
        backend:
          serviceName: gitlab
          servicePort: 8105
  - host: mattermost.tanuki.ayucloud.ovh
    http:
      paths:
      - path: /
        backend:
          serviceName: gitlab
          servicePort: 8065
---
